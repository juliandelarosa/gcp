aiofiles==23.1.0
aiohttp==3.8.4
aiosignal==1.3.1
alembic==1.10.4
allennlp==2.9.3
allennlp-models==2.9.3
anyio==3.6.2
apache-airflow==2.6.0
apache-airflow-providers-common-sql==1.4.0
apache-airflow-providers-ftp==3.3.1
apache-airflow-providers-http==4.3.0
apache-airflow-providers-imap==3.1.1
apache-airflow-providers-sqlite==3.3.2
apispec==5.2.2
appnope==0.1.3
argcomplete==3.0.8
argon2-cffi==21.3.0
argon2-cffi-bindings==21.2.0
arrow==1.2.3
asgiref==3.6.0
asttokens==2.2.1
async-timeout==4.0.2
attrs==23.1.0
autopep8==2.0.2
Babel==2.12.1
backcall==0.2.0
base58==2.1.1
beautifulsoup4==4.12.2
bertopic==0.14.1
bleach==6.0.0
blinker==1.6.2
blis==0.7.9
boto3==1.26.137
botocore==1.29.137
build==0.10.0
CacheControl==0.12.14
cached-path==1.1.2
cachelib==0.9.0
cachetools==5.3.0
catalogue==2.0.8
cattrs==22.2.0
certifi==2022.12.7
cffi==1.15.1
charset-normalizer==3.1.0
cleo==2.0.1
click==8.1.3
clickclick==20.10.2
colorama==0.4.6
colorlog==4.8.0
comm==0.1.3
confection==0.0.4
ConfigUpdater==3.1.1
conllu==4.4.1
connexion==2.14.2
contourpy==1.0.7
crashtest==0.4.1
cron-descriptor==1.2.35
croniter==1.3.14
crosslingual-coreference==0.3
cryptography==40.0.2
cssselect==1.2.0
cycler==0.11.0
cymem==2.0.7
Cython==0.29.34
databricks-api==0.9.0
databricks-cli==0.17.7
dataclasses==0.6
dataclasses-json==0.5.7
datasets==2.10.1
debugpy==1.6.7
decorator==5.1.1
defusedxml==0.7.1
Deprecated==1.2.13
dill==0.3.6
distlib==0.3.7
Django==4.2.4
django-admin-material-dashboard==1.0.10
django-material-admin==1.8.6
djangorestframework==3.14.0
dnspython==2.3.0
docker-pycreds==0.4.0
docutils==0.19
dulwich==0.21.5
email-validator==1.3.1
en-core-web-lg @ https://github.com/explosion/spacy-models/releases/download/en_core_web_lg-3.1.0/en_core_web_lg-3.1.0-py3-none-any.whl#sha256=57f57f20bc91bca79bd42c22f4f137eb728914553027bce88b616f4eeabd5d8c
en-core-web-md @ https://github.com/explosion/spacy-models/releases/download/en_core_web_md-3.1.0/en_core_web_md-3.1.0-py3-none-any.whl#sha256=45971f672a6312d493c0703dc4deeddde6e4f81ba98b626c8abf67b4bef720c7
en-core-web-sm @ https://github.com/explosion/spacy-models/releases/download/en_core_web_sm-3.6.0/en_core_web_sm-3.6.0-py3-none-any.whl#sha256=83276fc78a70045627144786b52e1f2728ad5e29e5e43916ec37ea9c26a11212
en-core-web-trf @ https://github.com/explosion/spacy-models/releases/download/en_core_web_trf-3.1.0/en_core_web_trf-3.1.0-py3-none-any.whl#sha256=d34003f4dcd6c6bc04347e00ec1145337e2c606f579f5b35a34089c74f624761
entities==1.0.0
es-core-news-sm @ https://github.com/explosion/spacy-models/releases/download/es_core_news_sm-3.6.0/es_core_news_sm-3.6.0-py3-none-any.whl#sha256=917d0b6359b27b222ccd616c47b43ee951f05da7402fdccce99364a465fa95d6
exceptiongroup==1.1.1
executing==1.2.0
fairscale==0.4.6
fastjsonschema==2.16.3
feedfinder2==0.0.4
feedparser==6.0.10
filelock==3.12.2
fire==0.5.0
Flask==2.2.5
Flask-AppBuilder==4.3.0
Flask-Babel==2.0.0
Flask-Caching==2.0.2
Flask-JWT-Extended==4.4.4
Flask-Limiter==3.3.1
Flask-Login==0.6.2
Flask-Session==0.4.1
Flask-SQLAlchemy==2.5.1
Flask-WTF==1.1.1
fonttools==4.40.0
fqdn==1.5.1
frozenlist==1.3.3
fsspec==2023.5.0
ftfy==6.1.1
future==0.18.3
gensim==4.3.1
gitdb==4.0.10
GitPython==3.1.31
google-api-core==2.11.0
google-auth==2.18.1
google-cloud-core==2.3.2
google-cloud-storage==2.9.0
google-crc32c==1.5.0
google-resumable-media==2.5.0
googleapis-common-protos==1.59.0
graphviz==0.20.1
gunicorn==20.1.0
h11==0.14.0
h5py==3.8.0
hdbscan==0.8.29
html5lib==1.1
html5tagger==1.3.0
httpcore==0.17.0
httplib2==0.22.0
httptools==0.5.0
httpx==0.24.0
huggingface-hub==0.14.1
idna==3.4
importlib-metadata==6.8.0
importlib-resources==5.12.0
inflection==0.5.1
iniconfig==2.0.0
inkpEntities @ file:///Users/juliandelarosa/Projects/Inkpills/lib/Entities/dist/inkpEntities-0.3.0-py3-none-any.whl#sha256=95092fb763191e3de1eb0394631bdfb7f17830b36abc25b431e5229106efae14
install==1.3.5
installer==0.7.0
ipykernel==6.22.0
ipython==8.12.0
ipython-genutils==0.2.0
ipywidgets==8.0.6
isoduration==20.11.0
itsdangerous==2.1.2
jaraco.classes==3.3.0
jedi==0.18.2
jieba3k==0.35.1
Jinja2==3.1.2
jmespath==1.0.1
joblib==1.2.0
johnsnowlabs==5.0.0
jsonnet==0.20.0
jsonpointer==2.3
jsonschema==4.17.3
jupyter==1.0.0
jupyter-console==6.6.3
jupyter-events==0.6.3
jupyter_client==8.2.0
jupyter_core==5.3.0
jupyter_server==2.5.0
jupyter_server_terminals==0.4.4
jupyterlab-pygments==0.2.2
jupyterlab-widgets==3.0.7
keyring==23.13.1
kiwisolver==1.4.4
langchain==0.0.154
langcodes==3.3.0
langdetect==1.0.9
lazy-object-proxy==1.9.0
limits==3.4.0
linkify-it-py==2.0.2
-e git+https://github.com/facebookresearch/llama.git@6c7fe276574e78057f917549435a2554000a876d#egg=llama
llvmlite==0.38.1
lmdb==1.4.1
lockfile==0.12.2
loguru==0.7.0
lxml==4.9.2
Mako==1.2.4
Markdown==3.4.3
markdown-it-py==2.2.0
MarkupSafe==2.1.2
marshmallow==3.19.0
marshmallow-enum==1.5.1
marshmallow-oneofschema==3.0.1
marshmallow-sqlalchemy==0.26.1
matplotlib==3.7.1
matplotlib-inline==0.1.6
mdit-py-plugins==0.3.5
mdurl==0.1.2
mistune==2.0.5
ml-datasets==0.2.0
more-itertools==9.1.0
mpmath==1.3.0
msgpack==1.0.5
multidict==6.0.4
multiprocess==0.70.14
murmurhash==1.0.9
mypy-extensions==1.0.0
nbclassic==0.5.5
nbclient==0.7.3
nbconvert==7.3.1
nbformat==5.8.0
nebula2-python @ file:///Users/juliandelarosa/Projects/nebula-python-2.0.0
neo4j==5.8.1
nest-asyncio==1.5.6
networkx==3.1
newspaper3k==0.2.8
nltk==3.8.1
nlu==4.2.2
notebook==6.5.4
notebook_shim==0.2.2
numba==0.55.2
numexpr==2.8.4
numpy==1.22.4
oauthlib==3.2.2
openai==0.27.5
openapi-schema-pydantic==1.2.4
ordered-set==4.1.0
packaging==23.1
pandas==2.0.1
pandocfilters==1.5.0
parso==0.8.3
path==16.6.0
pathspec==0.9.0
pathtools==0.1.2
pathy==0.10.1
pendulum==2.1.2
pexpect==4.8.0
pickleshare==0.7.5
Pillow==9.5.0
pkginfo==1.9.6
platformdirs==3.10.0
plotly==5.14.1
pluggy==1.0.0
poetry==1.5.1
poetry-core==1.6.1
poetry-plugin-export==1.4.0
preshed==3.0.8
prettytable==3.7.0
prison==0.2.1
prometheus-client==0.16.0
promise==2.3
prompt-toolkit==3.0.38
protobuf==3.20.3
psutil==5.9.5
psycopg==3.1.8
psycopg2-binary==2.9.6
ptyprocess==0.7.0
pure-eval==0.2.2
py-rouge==1.1
py4j==0.10.9
pyarrow==12.0.0
pyasn1==0.5.0
pyasn1-modules==0.3.0
pycodestyle==2.10.0
pycparser==2.21
pydantic==1.8.2
Pygments==2.15.1
PyJWT==2.6.0
pymongo==4.3.3
pynndescent==0.5.10
pyparsing==3.0.9
pyproject_hooks==1.0.0
pyrsistent==0.19.3
pyspark==3.1.2
pytest==7.3.1
python-daemon==3.0.1
python-dateutil==2.8.2
python-json-logger==2.0.7
python-nvd3==0.15.0
python-slugify==8.0.1
pytz==2023.3
pytzdata==2020.1
PyYAML==6.0
pyzmq==25.0.2
qtconsole==5.4.2
QtPy==2.3.1
rapidfuzz==2.15.1
regex==2023.3.23
requests==2.28.2
requests-file==1.5.1
requests-toolbelt==1.0.0
responses==0.18.0
rfc3339-validator==0.1.4
rfc3986-validator==0.1.1
rich==13.3.5
rich_argparse==1.1.0
rsa==4.9
s3transfer==0.6.1
sacremoses==0.0.53
sanic==23.3.0
sanic-routing==22.8.0
scikit-learn==1.2.2
scipy==1.10.1
seaborn==0.12.2
Send2Trash==1.8.0
sentence-transformers==2.2.2
sentencepiece==0.1.98
sentry-sdk==1.23.1
setproctitle==1.3.2
sgmllib3k==1.0.0
shellingham==1.5.0.post1
shortuuid==1.0.11
six==1.16.0
sklearn==0.0.post4
smart-open==6.3.0
smmap==5.0.0
sniffio==1.3.0
soupsieve==2.4.1
spacy==3.6.0
spacy-alignments==0.9.0
spacy-dbpedia-spotlight==0.2.6
spacy-entity-linker==1.0.3
spacy-legacy==3.0.12
spacy-loggers==1.0.4
spacy-transformers==1.0.6
spark-nlp==5.0.0
spark-nlp-display==4.1
SQLAlchemy==1.4.48
SQLAlchemy-JSONField==1.0.1.post0
SQLAlchemy-Utils==0.41.1
sqlparse==0.4.4
srsly==2.4.6
stack-data==0.6.2
style==1.1.0
svgwrite==1.4
sympy==1.11.1
tabulate==0.9.0
tenacity==8.2.2
tensorboardX==2.6
termcolor==1.1.0
terminado==0.17.1
text-unidecode==1.3
textblob==0.17.1
thinc==8.1.10
thinc-apple-ops==0.1.3
threadpoolctl==3.1.0
tiktoken==0.3.3
tinycss2==1.2.1
tinydb==4.7.1
tinysegmenter==0.3
tldextract==3.4.0
tokenizers==0.10.3
tomli==2.0.1
tomlkit==0.12.1
top2vec==1.0.29
torch==1.11.0
torchvision==0.12.0
tornado==6.3.1
tqdm==4.64.1
tracerite==1.1.0
traitlets==5.9.0
transformers==4.9.2
trove-classifiers==2023.7.6
typer==0.4.2
typing-inspect==0.8.0
typing_extensions==4.5.0
tzdata==2023.3
uc-micro-py==1.0.2
ujson==5.7.0
umap-learn==0.5.3
unicodecsv==0.14.1
update==0.0.1
uri-template==1.2.0
urllib3==1.26.15
uvloop==0.17.0
virtualenv==20.24.2
wandb==0.12.21
wasabi==0.10.1
wcwidth==0.2.6
webcolors==1.13
webencodings==0.5.1
websocket-client==1.5.1
websockets==11.0.3
Werkzeug==2.2.3
widgetsnbextension==4.0.7
wikipedia==1.4.0
Wikipedia-API==0.6.0
word2number==1.1
word2vec==0.11.1
wordcloud==1.9.2
wrapt==1.15.0
WTForms==3.0.1
xattr==0.10.1
xxhash==3.2.0
yarl==1.9.2
zipp==3.16.2
