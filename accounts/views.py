from django.shortcuts import render
from rest_framework import generics
from .models import Account
from .models import AccountSerializer


class CreateAccountView(generics.CreateAPIView):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

# Create your views here.
