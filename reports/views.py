from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, get_object_or_404, redirect
from .models import Report
from .forms import ReportForm  # Create this form in the next step

def report_list(request):
    reports = Report.objects.all()
    return render(request, 'report_app/report_list.html', {'reports': reports})

def report_detail(request, pk):
    report = get_object_or_404(Report, pk=pk)
    return render(request, 'report_app/report_detail.html', {'report': report})

def create_report(request):
    if request.method == 'POST':
        form = ReportForm(request.POST)
        if form.is_valid():
            report = form.save(commit=False)
            report.user = request.user  # Assuming you're using authentication
            report.save()
            return redirect('report_app:report_list')
    else:
        form = ReportForm()
    return render(request, 'report_app/report_form.html', {'form': form})
