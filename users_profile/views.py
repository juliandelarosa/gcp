from django.shortcuts import render
# users/views.py
from rest_framework import generics
from .models import UsersProfile, UsersProfileSerializer


class UserList(generics.ListCreateAPIView):
    queryset = UsersProfile.objects.all()
    serializer_class = UsersProfileSerializer
from django.shortcuts import render

# Create your views here.
