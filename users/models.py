from django.db import models
from django.contrib.auth.models import AbstractUser
# users/serializers.py
from rest_framework import serializers


class CustomUser(AbstractUser):
    mobile = models.PositiveBigIntegerField('mobile', null=True, blank=True)
    is_customer = models.BooleanField('is_customer', default=True)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['id', 'username', 'email', 'first_name', 'last_name', 'password', 'mobile', 'is_customer']

# Create your models here.
