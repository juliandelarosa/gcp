from django.db import models
from rest_framework import serializers

from users.models import CustomUser


class Account(models.Model):
    account_id = models.CharField(max_length=100)
    account_number = models.CharField(max_length=100)
    current_balance = models.DecimalField(max_digits=10, decimal_places=2)
    user_id = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

    def __str__(self):
        return self.account_number


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['account_id', 'account_number', 'current_balance', 'user_id']
