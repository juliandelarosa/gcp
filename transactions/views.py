from django.shortcuts import render
from rest_framework import generics
from .models import Transaction
from .models import TransactionSerializer


class TransactionList(generics.ListCreateAPIView):
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer

# Create your views here.
