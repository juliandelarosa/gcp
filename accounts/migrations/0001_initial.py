# Generated by Django 4.2.4 on 2023-08-05 18:17

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('account_id', models.CharField(max_length=100)),
                ('account_number', models.CharField(max_length=100)),
                ('current_balance', models.DecimalField(decimal_places=2, max_digits=10)),
            ],
        ),
    ]
