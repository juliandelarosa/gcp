from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth.models import User

from users.models import CustomUser

# Define choices for report types
REPORT_TYPES = (
    ('bug', 'Bug Report'),
    ('feature', 'Feature Request'),
    ('other', 'Other'),
)

class Report(models.Model):
    models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    report_type = models.CharField(max_length=20, choices=REPORT_TYPES)
    title = models.CharField(max_length=100)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
