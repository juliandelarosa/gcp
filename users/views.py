from django.shortcuts import render
# users/views.py
from rest_framework import generics
from .models import CustomUser
from .models import UserSerializer


class UserList(generics.ListCreateAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer

# Create your views here.
